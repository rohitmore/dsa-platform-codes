/*Code 1:Single Number
Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
Avizva, epam, cadence, paytm, atlassian,cultfit+7
Platform: LeetCode - 136
Striver’s SDE Sheet
Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.
*/

  class SingleNumber {
    public int singleNumber(int[] nums) {
        int result = 0;
        
        
        for (int num : nums) {
            result ^= num;
        }
        
        return result;
    }

    public static void main(String[] args) {
        SingleNumber singleNumberSolver = new SingleNumber();

        // Example 1
        int[] nums1 = {2, 2, 1};
        System.out.println("Example 1 Output: " + singleNumberSolver.singleNumber(nums1));

        // Example 2
        int[] nums2 = {4, 1, 2, 1, 2};
        System.out.println("Example 2 Output: " + singleNumberSolver.singleNumber(nums2));

        // Example 3
        int[] nums3 = {1};
        System.out.println("Example 3 Output: " + singleNumberSolver.singleNumber(nums3));
    }
}

