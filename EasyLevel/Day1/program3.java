/*
 * Code3: Key Pair
Company: Zoho, Flipkart, Morgan Stanley, Accolite, Amazon, Microsoft,
FactSet, Hike, Adobe, Google, Wipro, SAP Labs, CarWale
Platform: GFG
Description:
Given an array Arr of N positive integers and another number X. Determine
whether or not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105
 *
 * /
 *
 * ?
 */
import java.util.*;
/*class KeyPair{

	boolean checkPair(int sum,int arr[]){
           
	 	int i=0;
		while(i<arr.length){

			for(int j=0;j<arr.length;j++){

				if(arr[i]+arr[j]==sum){
					return true;
				}
			}

			i++;
		}

		return false;

	}


}
class Client{

	public static void main(String[] args){

		KeyPair obj=new KeyPair();
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter Array Size");

		int size=sc.nextInt();

		int arr[]=new int[size];
		System.out.println("Enter Array Elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter Sum That You want to check pairs");
		int sum=sc.nextInt();

		boolean flag=obj.checkPair(sum,arr);

		if(flag==true){
			System.out.println("Yes");
		}else{
			System.out.println("No");
	}

	}

}*/

class Solution{

	boolean keyPair(int arr[],int sum){
 
		HashSet<Integer> set=new HashSet<Integer>();
		for(int i=0;i<arr.length;i++){
                    set.add(arr[i]);
		}

		for(int j=0;j<arr.length;j++){
                   int temp=sum-arr[j];

		   if(set.contains(temp)){

			   return true;
		   }

		}

		return false;

	}

}

class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]={1,4};
		int sum=10;
		boolean flag=obj.keyPair(arr,sum);
		if(flag)
			System.out.println("Yes");
		else
			System.out.println("No");

	}

}
