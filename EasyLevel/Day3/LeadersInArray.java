/*Code2: Leaders in an Array
Company : PayU, Adobe, Microsoft, Synopsys, Coditas, Hashedln, Betsol
Platform : GFG
Description:

Given an array A of positive integers. Your task is to find the leaders in the
array. An element of an array is a leader if it is greater than or equal to all the
elements to its right side. The rightmost element is always a leader.

Example 1:
Input:
n = 6
A[] = {16,17,4,3,5,2}
Output: 17 5 2
Explanation: The first leader is 17 as it is greater than all the elements to its
right.
Similarly, the next leader is 5. The right most element is always a leader so it is
also included.
Example 2:
Input:
n = 5
A[] = {1,2,3,4,0}
Output: 4 0
Explanation: 0 is the rightmost element and 4 is the only element which is
greater
than all the elements to its right.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(n)
Constraints:
1 <= n <= 107
0 <= Ai <= 107
 *
 */
import java.util.*;
/*
class Solution{

	 void leadersArray(int []arr){

		 for(int i=0;i<arr.length;i++){

			 int temp=arr[i];
			 boolean flag=false;

			 for(int j=i+1;j<arr.length;j++){

				 if(arr[j]>temp){
					 flag=true;
					 break;
				 }
			 }
			 if(flag!=true){
			 System.out.print(temp +" ");
			 }

		 }

		 System.out.println();

	 }


}*/

class Solution{

	ArrayList<Integer> leadersArray(int []arr){

		ArrayList<Integer> al=new ArrayList<Integer>();
		int n=arr.length;
		int max=n-1;
                al.add(max);

		for(int i=n-2;i>=0;i--){

			if(max<=arr[i]){
				max=arr[i];
				al.add(max);
			}

		}

        	Collections.reverse(al);

		return al;
  

              }

}	

class Client{
	public static void main(String[] args){
         
		Solution obj=new Solution();
		int arr[]={1,2,3,4,0};
		System.out.println(obj.leadersArray(arr));

	}
}
