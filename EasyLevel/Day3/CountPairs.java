/*Code3 : Count pairs with given sum
Company: Amazon, MakeMyTri, Facebook, UnitedHealth Group
Platform: GFG
Love Babbar’s SDE Sheet
Description:
Given an array of N integers, and an integer K, find the number of pairs of
elements in the array whose sum is equal to K.
Example 1:

Input:
N = 4, K = 6
arr[] = {1, 5, 7, 1}
Output: 2
Explanation:
arr[0] + arr[1] = 1 + 5 = 6 and arr[1] + arr[3] = 5 + 1 = 6.
Example 2:
Input:
N = 4, K = 2
arr[] = {1, 1, 1, 1}
Output: 6
Explanation:
Each 1 will produce sum 2 with any 1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 105
1 <= K <= 108
1 <= Arr[i] <= 106
 *
 */
import java.util.*;
/*
class Solution{

	public int countPairs(int []arr,int sum){

		int count=0;

		for(int i=0;i<arr.length;i++){

			for(int j=i+1;j<arr.length;j++){

				if(arr[i]+arr[j]==sum){
					count++;
				}
			}
		}

		return count;

	}
}*/

class Solution{

	int getCountPairs(int []arr,int n,int k){

		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
		int count=0;

		for(int i=0;i<arr.length;i++){

			if(hm.containsKey(k-arr[i])){
				count+=hm.get(k-arr[i]);

			}else if(hm.containsKey(arr[i])){
				hm.put(arr[i],hm.get(arr[i])+1);
			}else{
				hm.put(arr[i],1);
			}

			}

		

		return count;

		}
           
  }


class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]={1,1,1,1};
		int k=2;
		int n=arr.length;
		int ret=obj.getCountPairs(arr,n,k);
                System.out.println(ret);

	}

}

