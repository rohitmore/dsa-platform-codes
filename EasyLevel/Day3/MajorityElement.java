/*Code 1: Majority Element
Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
Platform : Leetcode - 169, GFG
Fraz’s & striver’s SDE sheet.
Description

Given an array nums of size n, return the majority element.
The majority element is the element that appears more than [n / 2⌋ times. You
may assume that the majority element always exists in the array.
Example 1:
Input: nums = [3,2,3]
Output: 3
Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
Constraints:
n == nums.length
1 <= n <= 5 * 104
-109 <= nums[i] <= 109
 *
 * */
import java.util.*;
/*class Solution{

	public int majorityElement(int[]arr){

		int count1=0;
		int major=0;

		for(int i=0;i<arr.length;i++){
			int temp=arr[i];
			int count2=0;
			for(int j=0;j<arr.length;j++){

				if(arr[j]==temp){
					count2++;
				}
			}

			if(count2>count1){
				major=i;
			}

	}

	return arr[major];
	}
}*/

class Solution{

	public int majorityElement(int []nums){
		int length=nums.length/2;

		Arrays.sort(nums);

		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();

		for(int i=0;i<nums.length;i++){

			if(hm.containsKey(nums[i])){

			hm.put(nums[i],hm.get(nums[i])+1);

			}else{
				hm.put(nums[i],1);

			}

		if(hm.get(nums[i])>length){
			return nums[i];
		}

		}

		return 0;
	}
}

class Client{

	public static void main(String[] args){
		Solution obj=new Solution();
		int arr[]={3,3,4};
	int ret=obj.majorityElement(arr);

		System.out.println(ret);

	}
}
