/*Code3 : Equilibrium Point
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n non negative numbers. The task is to find the first
equilibrium point in an array. Equilibrium point in an array is an index (or position) such
that the sum of all elements before that index is the same as the sum of elements after
it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: The equilibrium point is at position 3 as the sum of elements
before it (1+3) = sum of elements after it (2+2).
Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation: Since there's only an element hence its only the equilibrium point.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
0 <= A[i] <= 109

*Solutions Will Be Provided Within 24 Hrs
 *
 */


class Solution{


    // a: input array
    // n: size of array
    // Function to find equilibrium point in the array.
    public  int equilibriumPoint(int arr[], int n) {

        for(int i=0;i<arr.length;i++){

		if(arr.length==1){
			return 1;
		}
		int leftsum=0;
		int rightsum=0;
		for(int j=0;j<arr.length;j++){
			if(j<i){
				leftsum=leftsum+arr[j];
			}else if(j>i){
				rightsum=rightsum+arr[j];
			}
		}
		if(leftsum==rightsum){
			return i+1;

		}

	}
	return -1;

    }
}

class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]={1,3,5,2,2};
		int n=arr.length;

		System.out.println(obj.equilibriumPoint(arr,n));

	}
}



