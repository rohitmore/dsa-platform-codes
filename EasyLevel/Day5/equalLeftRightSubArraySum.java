/*Code2 : Equal Left and Right Subarray Sum
Company : Amazon, Adobe
Platform : GFG
Description :
Given an array A of n positive numbers. The task is to find the first index in the
array such that the sum of elements before it equals the sum of elements after it.
Note: Array is 1-based indexed.
Example 1:
Input:
n = 5
A[] = {1,3,5,2,2}
Output: 3
Explanation: For second test case at position 3 elements before it (1+3) =
elements after it (2+2).

Example 2:
Input:

n = 1
A[] = {1}
Output: 1
Explanation: Since it's the only element hence it is the only point.
Expected Time Complexity: O(N)
Expected Space Complexity: O(1)
Constraints:
1 <= n <= 106
1 <= A[i] <= 108
 *
 */


class Solution {

    // a: input array

    // n: size of array

    // Function to find equilibrium point in the array.

    public static int leftRight(int arr[], int n) {
        
        int rightsum=0;
        int leftsum=0;
        
        if(arr.length==1){
            return 1;
        }
        
        for(int i=1;i<n;i++){
            rightsum+=arr[i];
        }
        
        for(int i=0;i<n-1;i++){
         if(leftsum==rightsum){
             return i+1;
             
         }   
         leftsum+=arr[i];
         rightsum-=arr[i+1];
         
         
        }
        
        return -1;
        
    }
    
}

class Client{

	public static void main(String[] args){

		int arr[]={1,3,5,2,2};
		int n=arr.length;

		System.out.println(Solution.leftRight(arr,n));

	}
}
