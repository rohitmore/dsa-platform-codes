/*Code 1: Two Sum

Company: Google, Meta, Amazon, Microsoft, Paypal+76 more companies
Platform : Leetcode - 1
Fraz’s & striver’s SDE sheet.
Description
Given an array of integers nums and an integer target, return indices of the two
numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use
the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]

Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
 */
/*import java.util.*;
class Solution{

	void twoSum(int arr[],int target){
		
		ArrayList<Integer> al=new ArrayList<Integer>();
		int i=0;

		while(i<arr.length){
			boolean flag=false;
			for(int j=0;j<arr.length;j++){
                                 
				if(arr[i]+arr[j]==target){
					al.add(i);
					al.add(j);
					flag=true;
					break;

				}
			}
			if(flag==true){
				Object arr1[]=al.toArray();
				for(int x=0;x<arr1.length;x++){
					System.out.print(arr1[x] +" ");
				}
                                System.out.println();
				return;
			}
			i++;
		}
	}
}
class Client{
	public static void main(String[] args){

		Solution obj=new Solution();
		int target=9;
		int arr[]={2,7,11,15};

		obj.twoSum(arr,target);

		/*for(var num:arr1){

			System.out.println(num);
		}



	}

}
*/


class Solution{

	int[] twoSum(int arr[],int target){

		int arr1[]=new int[2];
		boolean flag=false;

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){

				if(arr[i]+arr[j]==target){
					arr1[0]=i;
					arr1[1]=j;
			                flag=true;	
					
				}
                            if(flag==true){
				    return arr1;
			    }
				
			}


		}

		return arr1;
	}

}
class Client{
	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]=new int[]{2,7,11,15};
		int target=9;

	int arr2[]=obj.twoSum(arr,target);

	for(int num:arr2){
		System.out.print(num +" ");


	}

	System.out.println();

	}


}


