/*Code 1: Intersection of Two Arrays
Company: Accolite, Amazon, Microsoft, PayPal, Rockstand
Platform: Leetcode - 349
Striver’s SDE sheet
Given two integer arrays nums1 and nums2, return an array of their intersection.
Each element in the result must be unique and you may return the result in any
order.
Example 1:
Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]
Example 2:
Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.
Constraints:
1 <= nums1.length, nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 1000
 *
 */

import java.util.*;
class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> hs=new HashSet<>();
        for(int i=0;i<nums1.length;i++){
               hs.add(nums1[i]);
        }
        ArrayList<Integer> al=new ArrayList<>();
        for(int i=0;i<nums2.length;i++){
            if(hs.contains(nums2[i])){
                al.add(nums2[i]);
                hs.remove(nums2[i]);
            }
        }
        int arr[]=new int[al.size()];
        for(int i=0;i<arr.length;i++){
            arr[i]=al.get(i);
        }
        return arr;
    }
}

class Client{

	public static void main(String[] args){
		Solution obj=new Solution();
		int[] nums1 = {4,9,5};
	        int[] nums2={9,4,9,8,4};

		int arr[]=obj.intersection(nums1,nums2);

		System.out.println(Arrays.toString(arr));

	}
}
