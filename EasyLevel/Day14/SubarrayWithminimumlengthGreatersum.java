/*Code 3: Smallest subarray with sum greater than x
Company: Accolite, Amazon, Goldman Sachs, Google, Facebook

Platform: GFG
Given an array of integers (A[]) and a number x, find the smallest subarray with
sum greater than the given value. If such a subarray do not exist return 0 in that
case.
Example 1:
Input:
A[] = {1, 4, 45, 6, 0, 19}
x = 51
Output: 3
Explanation:
Minimum length subarray is
{4, 45, 6}

Example 2:
Input:
A[] = {1, 10, 5, 2, 7}
x = 9
Output: 1
Explanation:
Minimum length subarray is {10}

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N, x ≤ 105
0 ≤ A[] ≤ 104
 *
 */

import java.util.*;

class Solution {

    public static int smallestSubWithSum(int a[], int n, int x) {
          int min_len = n + 1;
        int start = 0, end = 0;
        int curr_sum = 0;

        while (end < n){
            while (curr_sum <= x && end < n) {
                if (curr_sum <= 0 && x > 0) {
                    
                    start = end;
                    curr_sum = 0;
                }

                curr_sum += a[end];
                end++;
            }

                        while (curr_sum > x && start < n) {
                if (end - start < min_len) {
                    min_len = end - start;
                }

                curr_sum -= a[start];
                start++;
            }
        }

        return (min_len == n + 1) ? 0 : min_len;
    }
}


class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]={1, 3, 4, 7, 10, 10, 8, 10};

		System.out.println(obj.smallestSubWithSum(arr,arr.length,10));

	}
}
