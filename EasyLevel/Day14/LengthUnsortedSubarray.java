/*Code 2: Length Unsorted Subarray
Company: Flipkart, Microsoft, Adobe, Make my trip
Platform: GFG
Given an unsorted array Arr of size N. Find the subarray Arr[s...e] such that
sorting this subarray makes the whole array sorted.

Example 1:
Input:
N = 11
Arr[] ={10,12,20,30,25,40,32,31,35,50,60}
Output: 3 8
Explanation: Subarray starting from index
3 and ending at index 8 is required
subarray. Initial array: 10 12 20 30 25
40 32 31 35 50 60 Final array: 10 12 20
25 30 31 32 35 40 50 60
(After sorting the bold part)
Example 2:
Input:
N = 9
Arr[] = {0, 1, 15, 25, 6, 7, 30, 40, 50}
Output: 2 5
Explanation: Subarray starting from index
2 and ending at index 5 is required
Subarray.
Initial array: 0 1 15 25 6 7 30 40 50
Final array: 0 1 6 7 15 25 30 40 50
(After sorting the bold part)
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 107
1 ≤ Arr[i] ≤ 108
 *
 */

import java.util.*;

class Solve {
    int[] printUnsorted(int[] arr, int n) {
       ArrayList<Integer> al=new ArrayList<>();
      int arr1[]=new int[2];
      if(arr.length==1){
           arr1[0]=0;
           arr1[1]=0;
           return arr1;
       }
       for(int i=0;i<n;i++){
           al.add(arr[i]);
       }

       Arrays.sort(arr);
       ArrayList<Integer> index=new ArrayList<>();
       for(int i=0;i<arr.length;i++){
           if(arr[i]!=al.get(i)){
               index.add(i);
           }
       }

       arr1[0]=index.get(0);
       arr1[1]=index.get(index.size()-1);

       return arr1;
    }
}

class Client{

	public static void main(String[] args){

		Solve obj=new Solve();
       int Arr[] ={10,12,20,30,25,40,32,31,35,50,60};

       int arr1[]=obj.printUnsorted(Arr,Arr.length);

       System.out.println(Arrays.toString(arr1));

	}
}

