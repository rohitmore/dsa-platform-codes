/*
 *Easy Level - Day 4

Code 1: Max Consecutive Ones

Company: Google, Facebook, Amazon, Microsoft, Apple, Uber, Airbnb, Adobe, Goldman Sachs,
Bloomberg
Platform: Leetcode - 485, Coding Ninja
Striver’s SDE Sheet
Description:
Given a binary array nums, return the maximum number of consecutive 1's in the array.
Example 1:
Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The
maximum number of consecutive 1s is 3.
Example 2:
Input: nums = [1,0,1,1,0,1]
Output: 2

Constraints:
1 <= nums.length <= 105
nums[i] is either 0 or 1.
 *
 */

import java.util.*;
class Solution{

	int maxCons1(int arr[]){

		int count=0;
		int maxCount=0;

		for(int i=0;i<arr.length;i++){

			if(arr[i]==1){
				count++;
				maxCount=Math.max(maxCount,count);
			}
			else{
				count=0;
			}
		}

		return maxCount;

	}

}

class Client{

	public static void main(String[] args){

		int arr[]={1,0,1,1,0,1};
		Solution obj=new Solution();

		System.out.println(obj.maxCons1(arr));

	}
}
