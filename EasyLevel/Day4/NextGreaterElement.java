/*Code 2: Next Greater Element I

Company: Flipkart, Amazon, Microsoft, MakeMyTrip, Adobe
Platform: Leetcode - 496, GFG
Description:

The next greater element of some element x in an array is the first greater element that
is to the right of x in the same array.
You are given two distinct 0-indexed integer arrays nums1 and nums2, where nums1 is a
subset of nums2.
For each 0 <= i < nums1.length, find the index j such that nums1[i] == nums2[j] and
determine the next greater element of nums2[j] in nums2. If there is no next greater
element, then the answer for this query is -1.
Return an array ans of length nums1.length such that ans[i] is the next greater element
as described above.

Example 1:
Input: nums1 = [4,1,2], nums2 = [1,3,4,2]
Output: [-1,3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 4 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.
- 1 is underlined in nums2 = [1,3,4,2]. The next greater element is 3.
- 2 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the
answer is -1.

Example 2:
Input: nums1 = [2,4], nums2 = [1,2,3,4]
Output: [3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 2 is underlined in nums2 = [1,2,3,4]. The next greater element is 3.
- 4 is underlined in nums2 = [1,2,3,4]. There is no next greater element, so the
answer is -1.

Constraints:
1 <= nums1.length <= nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 104
All integers in nums1 and nums2 are unique.
All the integers of nums1 also appear in nums2.
 *
 *
 */



import java.util.*;
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {

	    int arr[]=new int[nums1.length];
	    int k=0;

	    for(int i=0;i<nums1.length;i++){

		    boolean flag=false;
		    int j=0;

		    while(j<nums2.length){
			    if(nums2[j]==nums1[i]){
				    int l=j+1;
				    while(l<nums2.length){
			    if(nums2[l]>nums1[i]){

				    arr[k]=nums2[l];
				    flag=true;
				    k++;
				    break;
		                        	    }
			    
                        l++;
				    }
 
			
			    }

			    if(flag==true){
				    break;
			    }
			    j++;
		    }
		    if(flag==false){
			    arr[k]=-1;
			    k++;
		    }
	    }

	    return arr;
                                       
   }

}
class Client{

	public static void main(String[] args){
		Solution obj=new Solution();
		int arr1[]={4,1,2};
		int arr2[]={1,3,4,2};

    	     int arr3[]=obj.nextGreaterElement(arr1,arr2);

	     System.out.println(Arrays.toString(arr3));

	}
}
