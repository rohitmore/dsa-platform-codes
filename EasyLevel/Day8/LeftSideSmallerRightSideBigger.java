/*Code2: Element with left side smaller and right side greater
Company: Zoho, Amazon, OYO Rooms, Intuit
Platform: GFG
Description :
Given an unsorted array of size N. Find the first element in the array such that all
of its left elements are smaller and all right elements are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.

Example 1:
Input:
N = 4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on left of 5 are smaller than 5
and on right of it are greater than 5.

Example 2:
Input:
N = 3
A[] = {11, 9, 12}
Output:
-1

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:
3 <= N <= 106
1 <= A[i] <= 106
 *
 */

import java.util.*;

class Compute {
    public int findElement(int arr[], int n){

            int leftMax[]=new int[n];
            int rightMin[]=new int[n];
            int result=-1;

            leftMax[0]=arr[0];

            for(int i=1;i<n;i++){
                leftMax[i]=Math.max(leftMax[i-1],arr[i-1]);
            }
            rightMin[n-1]=arr[n-1];

            for(int i=n-2;i>=0;i--){
                rightMin[i]=Math.min(rightMin[i+1],arr[i+1]);
            }

	    System.out.println(Arrays.toString(leftMax));
	    System.out.println(Arrays.toString(rightMin));

            for(int i=1;i<n-1;i++){
                if(arr[i]>=leftMax[i]&&arr[i]<=rightMin[i]){
                    result=arr[i];
                    break;
                }
            }
            return result;
    }
}

class Client{

	public static void main(String[] args){
		int arr[]={4,2,5,7};
		int n=arr.length;
		Compute obj=new Compute();

		System.out.println(obj.findElement(arr,n));

	}
}
