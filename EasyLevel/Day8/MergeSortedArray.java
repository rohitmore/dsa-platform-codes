/*Code 1: Merge Sorted Array
Company: Amazon, Samsung, LinkedIn +50 companies
Platform: Leetcode - 88
Fraz’s and Striver’s SDE sheet
Description:
You are given two integer arrays nums1 and nums2, sorted in non-decreasing order,
and two integers m and n, representing the number of elements in nums1 and nums2
respectively.
Merge nums1 and nums2 into a single array sorted in non-decreasing order.
The final sorted array should not be returned by the function, but instead be stored
inside the array nums1. To accommodate this, nums1 has a length of m + n, where the
first m elements denote the elements that should be merged, and the last n elements
are set to 0 and should be ignored. nums2 has a length of n.
Example 1:
Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
Output: [1,2,2,3,5,6]
Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
The result of the merge is [1,2,2,3,5,6] with the underlined elements coming from
nums1.
Example 2:
Input: nums1 = [1], m = 1, nums2 = [], n = 0
Output: [1]
Explanation: The arrays we are merging are [1] and [].
The result of the merge is [1].
Example 3:
Input: nums1 = [0], m = 0, nums2 = [1], n = 1
Output: [1]
Explanation: The arrays we are merging are [] and [1].
The result of the merge is [1].
Note that because m = 0, there are no elements in nums1. The 0 is only there to ensure
the merge result can fit in nums1.

Constraints:
nums1.length == m + n
nums2.length == n
0 <= m, n <= 200
1 <= m + n <= 200
-109 <= nums1[i], nums2[j] <= 109
 *
 */
import java.util.*;
/*1
class Solution {

    public void merge(int[] nums1, int m, int[] nums2, int n) {
     
     int arr[]=new int[m];
     for(int i=0;i<m;i++){
         arr[i]=nums1[i];
     }
        int i = 0;

        int j = 0;

        int k = 0;

        while (i < m && j < n) {

            if (arr[i] < nums2[j]) {

                nums1[k] = arr[i];

                i++;

            } else {

                nums1[k] = nums2[j];

                j++;

            }

            k++;

        }

        while (i < m) {

            nums1[k] = arr[i];

            i++;

            k++;

        }

        while (j < n) {

            nums1[k] = nums2[j];

            j++;

            k++;

        }

        System.out.println(Arrays.toString(nums1));

    }

}*/

//more optimise solution

class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {

      //  [1,2,3,0,0,0]
     //   m=3
     //   [2,5,6]
    //    n=3

      //  [1,2,2,3,5,6]

        for(int i=0;i<n;i++)
        {
            nums1[m+i]=nums2[i];
        }
        Arrays.sort(nums1);
        System.out.println(Arrays.toString(nums1));
    }
}

class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr1[]={1,2,3,0,0,0};
		int m=3;
		int arr2[]={2,5,6};
		int n=3;

		obj.merge(arr1,m,arr2,n);

	}
}

