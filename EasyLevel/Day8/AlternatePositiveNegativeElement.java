/*Code3 : Alternate positive and negative numbers
Company : Paytm, VMWare, Amazon, Microsoft, Intuit, Hotstar, PayU, FourKites, KPIT,
CodeNation
Love Bubbar’s SDE Sheet.
Platform : GFG, Coding Ninjas
Description :
Given an unsorted array Arr of N positive and negative numbers. Your task is to
create an array of alternate positive and negative numbers without changing the relative
order of positive and negative numbers.
Note: Array should start with a positive number and 0 (zero) should be considered a
positive element.
Example 1:
Input:
N = 9
Arr[] = {9, 4, -2, -1, 5, 0, -5, -3, 2}
Output:
9 -2 4 -1 5 -5 0 -3 2
Explanation :
Positive elements : 9,4,5,0,2
Negative elements : -2,-1,-5,-3
As we need to maintain the relative order of
positive elements and negative elements we will pick
each element from the positive and negative and will
store them. If any of the positive and negative numbers
are completed. we will continue with the remaining signed

elements.The output is 9,-2,4,-1,5,-5,0,-3,2.
Example 2:
Input:
N = 10
Arr[] = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8}
Output:
5 -5 2 -2 4 -8 7 1 8 0
Explanation :
Positive elements : 5,2,4,7,1,8,0
Negative elements : -5,-2,-8
As we need to maintain the relative order of
positive elements and negative elements we will pick
each element from the positive and negative and will
store them. If any of the positive and negative numbers
are completed. we will continue with the remaining signed
elements.The output is 5,-5,2,-2,4,-8,7,1,8,0.
Your Task:
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:
1 ≤ N ≤ 107
-106 ≤ Arr[i] ≤ 107
 *
 */
import java.util.*;


class Solution {
    void rearrange(int arr[], int n) {

	    ArrayList<Integer> positive=new ArrayList<Integer>();
	    ArrayList<Integer> negative=new ArrayList<Integer>();

	    for(int i=0;i<arr.length;i++){

		    if(arr[i]<0){

			    negative.add(arr[i]);
		    }else if(arr[i]>=0){
			    positive.add(arr[i]);
		    }
	    }

	    int i=0;
	    int j=0;
	    int k=0;

	    while(i<positive.size() && j<negative.size()){

		    if(k%2==0){
			    arr[k]=positive.get(i);
			    i++;
		    }else{
			    arr[k]=negative.get(j);
			    j++;
		    }
		    k++;
	    }

	    while(i<positive.size()){
		    arr[k]=positive.get(i);
		    i++;
		    k++;
	    }
	    while(j<negative.size()){
		    arr[k]=negative.get(j);
		    j++;
		    k++;
	    }

	    System.out.println(Arrays.toString(arr));


   }

}

class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int arr[]={9, 4, -2, -1, 5, 0, -5, -3, 2};
		int n=arr.length;

		obj.rearrange(arr,n);
	}
}



