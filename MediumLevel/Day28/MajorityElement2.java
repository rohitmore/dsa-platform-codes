/*Code 1: Majority Element II
Company:google,amazon, morgan stanley,Genpact, coditas
Platform- leetcode-229
Striver’s DSA sheet.
Description:
Given an integer array of size n, find all elements that appear more than ⌊
n/3 ⌋ times.

Example 1:
Input: nums = [3,2,3]
Output: [3]
Example 2:
Input: nums = [1]
Output: [1]
Example 3:
Input: nums = [1,2]
Output: [1,2]

Constraints:
1 <= nums.length <= 5 * 104
-10^9 <= nums[i] <= 10^9
 *
 */
import java.util.*;
class Solution {
    public List<Integer> majorityElement(int[] nums) {

	    int count=nums.length/3;

	    HashMap<Integer,Integer> hm=new HashMap<>();
	  //  List<Integer> al=new ArrayList<>();
	     HashSet<Integer> hs=new HashSet<>();

	    for(int i=0;i<nums.length;i++){
		    if(hm.containsKey(nums[i])){
			    hm.put(nums[i],hm.get(nums[i])+1);
		    }else{
			    hm.put(nums[i],1);
		    }

		    if(hm.get(nums[i])>count){
			    hs.add(nums[i]);
			    hm.remove(nums[i]);
		    }
	    }

	   // HashSet<Integer> hs=new HashSet<>();
	    List<Integer> al=new ArrayList<>(hs);



	    return al;
    }
}
class Client{

	public static void main(String[] args){

		Solution obj=new Solution();
		int[] nums = {2,2};

  	System.out.println(obj.majorityElement(nums));
	}
}
