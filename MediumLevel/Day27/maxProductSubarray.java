/*Code 1: Maximum Product Subarray

Company : Morgan Stanley, Amazon, Microsoft, OYO Rooms, D-E-Shaw, Google
Platform: leetcode - 152
Striver’s DSA sheet
Description:
Given an integer array nums, find a
subarray
that has the largest product, and return the product.
The test cases are generated so that the answer will fit in a 32-bit integer.
Example 1:
Input: nums = [2,3,-2,4]
Output: 6
Explanation: [2,3] has the largest product 6.
Example 2:
Input: nums = [-2,0,-1]
Output: 0
Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
Constraints:
1 <= nums.length <= 2 * 104
-10 <= nums[i] <= 10
The product of any prefix or suffix of nums
 *
 */

import java.util.*;
class Solution {
    public int maxProduct(int[] nums) {
        int maxProduct=nums[0];
        int leftProduct=1;
        int rightProduct=1;
        int n=nums.length;

        for(int i=0;i<nums.length;i++){

            //if any of left or right product gets zero then remainning all product are
            //getting zero since we have to take care of this.
           leftProduct=(leftProduct==0)?1:leftProduct;
           rightProduct=(rightProduct==0)?1:rightProduct;

           leftProduct*=nums[i];
           rightProduct*=nums[n-1-i];
  //Here we are checking maxProduct between privious maxProduct and from leftProduct and rigthProduct
           maxProduct=Math.max(maxProduct,Math.max(leftProduct,rightProduct));
        }

       return maxProduct;
    }
}
