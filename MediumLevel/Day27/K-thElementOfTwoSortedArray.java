/*(
 *Code 2: K-th element of two Arrays
Company: Flipkart, Microsoft
Platform: GFG
Love Babbar’s DSA Sheet
Description:
Given two sorted arrays arr1 and arr2 of size N and M respectively and an element
K. The task is to find the element that would be at the kth position of the final
sorted array.

Example 1:
Input:
arr1[] = {2, 3, 6, 7, 9}
arr2[] = {1, 4, 8, 10}
k = 5
Output: 6
Explanation: The final sorted array would be - 1, 2, 3, 4, 6, 7, 8, 9, 10
The 5th element of this array is 6.
Example 2:
Input:
arr1[] = {100, 112, 256, 349, 770}
arr2[] = {72, 86, 113, 119, 265, 445, 892}
k = 7
Output: 256
Explanation: Final sorted array is - 72, 86, 100, 112, 113, 119, 256, 265, 349,
445, 770, 892
7th element of this array is 256.
Expected Time Complexity: O(Log(N) + Log(M))
Expected Auxiliary Space: O(Log (N))

Constraints:
1 <= N, M <= 106
0 <= arr1i, arr2i < INT_MAX
1 <= K <= N+M
 *
 */
import java.util.*;
class Solution {
    public int kthElement( int arr1[], int arr2[], int n, int m, int k) {
        
        int arr3[]=new int[arr1.length+arr2.length];
        int i=0;
        int j=0;
        int b=0;
        
        while(i<arr1.length&&j<arr2.length){
           
        	if(arr1[i]<arr2[j]){
                arr3[b]=arr1[i];
                i++;
                  }else{
                arr3[b]=arr2[j];
                j++;
            }
       
        b++;
            
        }
        
        while(i<n){
            arr3[b]=arr1[i];
            i++;
            b++;
        }
        
        while(j<m){
            arr3[b]=arr2[j];
            j++;
            b++;
        }
         System.out.println(Arrays.toString(arr3));
        return arr3[k-1];
    }
}

class Client{

	public static void main(String[] args){

		int arr1[] = {2, 3, 6, 7, 9};
                int arr2[] = {1, 4, 8, 10};
                int k = 5;
              Solution obj=new Solution();
		System.out.println(obj.kthElement(arr1,arr2,arr1.length,arr2.length,k));

	}
}

