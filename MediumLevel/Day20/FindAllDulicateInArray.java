/*Code 1: Find All Duplicates in an Array
Company: Amazon, Microsoft, OYO Rooms
Platform: Leetcode - 442
Fraz’s DSA sheet.
Description:
Given an integer array nums of length n where all the integers of nums are in the range
[1, n] and each integer appears once or twice, return an array of all the integers that appears
twice.
You must write an algorithm that runs in O(n) time and uses only constant extra space.
Example 1:
Input: nums = [4,3,2,7,8,2,3,1]
Output: [2,3]
Example 2:
Input: nums = [1,1,2]
Output: [1]
Example 3:
Input: nums = [1]
Output: []

Constraints:
n == nums.length
1 <= n <= 105
1 <= nums[i] <= n
 *
 */
import java.util.*;

class Solution {
    public List<Integer> findDuplicates(int[] nums) {

        List<Integer> al=new ArrayList<Integer>();
        HashMap<Integer,Integer> hm=new HashMap<>();
        //Here if size of Array ia 1 then there no possibility of containing duplicate so retuen empty list
        if(nums.length==1){
            return new ArrayList<Integer>();
        }
         //here in this for loop we are storing data in hashmap and there frequency in from of key values
        for(int i=0;i<nums.length;i++){
            //if any of key is Already present
            if(hm.containsKey(nums[i])){
                hm.put(nums[i],hm.get(nums[i])+1);
            }else{
                //if key is not present already
                hm.put(nums[i],1);
            }
            //If frequency of any key is greater than 1 then add this key in list
             if(hm.get(nums[i])>1){
               al.add(nums[i]);
               hm.put(nums[i],0);
           }
        }

	return al;
    
    }
}
