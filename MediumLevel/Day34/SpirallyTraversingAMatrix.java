/*Code 2: Spirally traversing a matrix
Company: Paytm, Flipkart, Morgan Stanley, Microsoft, OYO Rooms, Snapdeal,
D-E-Shaw, MakeMyTrip, OracleMAQ Software, Adobe,nearbuy, NagarroBrowser,
Stack, Salesforce
Platform: GFG
love babbar DSA sheet
Description:
Given a matrix of size r*c. Traverse the matrix in spiral form.
Example 1:
Input:
r = 4, c = 4
matrix[][] = {{1, 2, 3, 4},
{5, 6, 7, 8},
{9, 10, 11, 12},
{13, 14, 15,16}}
Output: 1 2 3 4 8 12 16 15 14 13 9 5 6 7 11 10
Explanation:
Example 2:
Input:
r = 3, c = 4
matrix[][] = {{1, 2, 3, 4},
{5, 6, 7, 8},
{9, 10, 11, 12}}
Output: 1 2 3 4 8 12 11 10 9 5 6 7

Explanation: Applying same technique as shown above, output for the
2nd testcase will be 1 2 3 4 8 12 11 10 9 5 6 7.
Expected Time Complexity: O(r*c)
Expected Auxiliary Space: O(r*c), for returning the answer only.
Constraints:
1 <= r, c <= 100
0 <= matrixi <= 100
 *
 */
import java.util.*;

class Solution
{
    //Function to return a list of integers denoting spiral traversal of matrix.
    static ArrayList<Integer> spirallyTraverse(int matrix[][], int r, int c)
    {
     if (matrix.length == 0)
      return new ArrayList<>();

    final int m = matrix.length;
    final int n = matrix[0].length;
    ArrayList<Integer> ans = new ArrayList<>();
    int r1 = 0;
    int c1 = 0;
    int r2 = m - 1;
    int c2 = n - 1;

    // Repeatedly add matrix[r1..r2][c1..c2] to `ans`.
    while (ans.size() < m * n) {
      for (int j = c1; j <= c2 && ans.size() < m * n; ++j)
        ans.add(matrix[r1][j]);
      for (int i = r1 + 1; i <= r2 - 1 && ans.size() < m * n; ++i)
        ans.add(matrix[i][c2]);
      for (int j = c2; j >= c1 && ans.size() < m * n; --j)
        ans.add(matrix[r2][j]);
      for (int i = r2 - 1; i >= r1 + 1 && ans.size() < m * n; --i)
        ans.add(matrix[i][c1]);
      ++r1;
      ++c1;
      --r2;
      --c2;
    }

    return ans;
    }
}

