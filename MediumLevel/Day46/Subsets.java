/*Medium Level: Day 46
Code 1: Subsets
Company: Amazon, Whitehat, Wipro, VMWare,Travelopia
Platform : leetcode-78, GFG
Fraz’s DSA Sheet
Description:
Given an integer array nums of unique elements, return all possible
subsets
(the power set).
The solution set must not contain duplicate subsets. Return the solution in any order.
Example 1:
Input: nums = [1,2,3]
Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
Example 2:
Input: nums = [0]
Output: [[],[0]]
Constraints:
1 <= nums.length <= 10
-10 <= nums[i] <= 10
All the numbers of nums are unique.

Code 2:Stickler Thief
Company: Microsoft
Platform: GFG
Love Babber’s DSA Sheet
Description:
Stickler the thief wants to loot money from a society having n houses in a single line. He
is a weird person and follows a certain rule when looting the houses. According to the
rule, he will never loot two consecutive houses. At the same time, he wants to maximize
the amount he loots. The thief knows which house has what amount of money but is
unable to come up with an optimal looting strategy. He asks for your help to find the

maximum money he can get if he strictly follows the rule. ith house has a[i] amount of
money present in it.
Example 1:
Input:
n = 5
a[] = {6,5,5,7,4}
Output: 15
Explanation: Maximum amount he can get by looting 1st, 3rd and 5th house. Which is
6+5+4=15.
Example 2:
Input:
n = 3
a[] = {1,5,3}
Output: 5
Explanation: Loot only 2nd house and get maximum amount of 5.
Expected Time Complexity: O(N).
Expected Space Complexity: O(1).
Constraints:
1 ≤ n ≤ 105
1 ≤ a[i] ≤ 104
 *
 */

import java.util.*;
class Solution {
  public List<List<Integer>> subsets(int[] nums) {
    List<List<Integer>> ans = new ArrayList<>();
    dfs(nums, 0, new ArrayList<>(), ans);
    return ans;
  }

  private void dfs(int[] nums, int s, List<Integer> path, List<List<Integer>> ans) {
    ans.add(new ArrayList<>(path));

    for (int i = s; i < nums.length; ++i) {
      path.add(nums[i]);
      dfs(nums, i + 1, path, ans);
      path.remove(path.size() - 1);
    }
  }
}
