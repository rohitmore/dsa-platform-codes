/*Code 1: Best Time to Buy and Sell Stock II
Companies: Amazon, Facebook, Atlassian, OYO, LinkedIn, Infosys, PhonePe, HCL
Platforms: leetcode - 122
Fraz’s DSA sheet
Description:
You are given an integer array of prices where prices[i] is the price of a given stock on
the ith day.
On each day, you may decide to buy and/or sell the stock. You can only hold at most one
share of the stock at any time. However, you can buy it and then immediately sell it on
the same day.
Find and return the maximum profit you can achieve.
Example 1:
Input: prices = [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
Total profit is 4 + 3 = 7.
Example 2:
Input: prices = [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
Total profit is 4.
Example 3:
Input: prices = [7,6,4,3,1]
Output: 0
Explanation: There is no way to profit positively, so we never buy the stock to achieve
the maximum profit of 0.

Constraints:
1 <= prices.length <= 3 * 104
0 <= prices[i] <= 104
 *
 */
import java.util.*;
class Solution {
  public int maxProfit(int[] prices) {
    int sell = 0;
    int hold = Integer.MIN_VALUE;

    for (final int price : prices) {
      sell = Math.max(sell, hold + price);
      hold = Math.max(hold, sell - price);
    }

    return sell;
  }
}
