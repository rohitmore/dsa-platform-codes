/*Code 2: Sliding Window Maximum
Company: Flipkart, Amazon, Microsoft, Directi
Platform: leetcode-239
Fraz’s DSA sheet

Description:
You are given an array of integers nums, there is a sliding window of size k which is
moving from the very left of the array to the very right. You can only see the k numbers in
the window. Each time the sliding window moves right by one position.
Return the max sliding window.

Example 1:
Input:
nums = [1,3,-1,-3,5,3,6,7], k = 3
Output:
[3,3,5,5,6,7]

Explanation:
Window position Max
--------------- -----
[1 3 -1] -3 5 3 6 7 3
1 [3 -1 -3] 5 3 6 7 3
1 3 [-1 -3 5] 3 6 7 5
1 3 -1 [-3 5 3] 6 7 5
1 3 -1 -3 [5 3 6] 7 6
1 3 -1 -3 5 [3 6 7] 7
Example 2:
Input:
nums = [1], k = 1
Output:
[1]

Constraints:
1 <= nums.length <= 105
-104 <= nums[i] <= 104
1 <= k <= nums.length
 *
 */
import java.util.*;
class Solution {
  public int[] maxSlidingWindow(int[] nums, int k) {
    int[] ans = new int[nums.length - k + 1];
    Deque<Integer> maxQ = new ArrayDeque<>();

    for (int i = 0; i < nums.length; ++i) {
      while (!maxQ.isEmpty() && maxQ.peekLast() < nums[i])
        maxQ.pollLast();
      maxQ.offerLast(nums[i]);
      if (i >= k && nums[i - k] == maxQ.peekFirst()) // out-of-bounds
        maxQ.pollFirst();
      if (i >= k - 1)
        ans[i - k + 1] = maxQ.peekFirst();
    }

    return ans;
  }
}
